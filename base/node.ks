#


# Keyboard layouts
keyboard --vckeymap=latam --xlayouts='latam'
# System language
lang en_US.UTF-8
# System timezone
timezone America/Argentina/Cordoba
# System authorization information
authselect --enableshadow --passalgo=sha512
# Root password
#python3 -c 'import crypt; print(crypt.crypt("ChangeMe", crypt.mksalt()))'
rootpw --iscrypted $6$T9.ibGxPO9AVrurU$emMdmGX4arPiT/6xKBWSeMs47kp/Rbxz15IqRF3oHh8gUVoDnEvOhZwORychHIFYDJ6DzYaBrGfhAUvNtcVwt0

#Selinux
selinux --disabled

# Network information
network --bootproto=dhcp --device=link --noipv6 --activate --onboot=yes
network  --hostname=nodocba.localhost
#TODO: CHANGE
#network  --bootproto=static --device=eno1 --ip=X.X.X.X --netmask=255.255.255.0 --gateway=X.X.X.X --nameserver=X.X.X.X --noipv6 --activate
#network  --hostname=nodocba.localhost

# install via http
text
url --url="http://dl.rockylinux.org/vault/rocky/8.6/BaseOS/x86_64/os"
repo --name=appstream --baseurl="http://dl.rockylinux.org/vault/rocky/8.6/AppStream/x86_64/os"
eula --agreed
reboot

# System bootloader configuration
bootloader --location=mbr --boot-drive=sda
# Partition clearing information
ignoredisk --only-use=sda
clearpart --drives=sda --all
zerombr 
# Disk partitioning information
part /boot --fstype="ext4" --ondisk=sda --size=1024
part pv.01 --fstype="lvmpv" --ondisk=sda --size=1 --grow
volgroup vg_server --pesize=4 pv.01
logvol /  --fstype="xfs" --size=6000 --name=lv_root --vgname=vg_server
# Recommended swap size = (0.5 * RAM) + (overcommit ratio * RAM)
# 0.5 * 24 + ((12 + 12) / 24) * 24 = 36
logvol swap --fstype="swap" --size=1000 --name=lv_swap --vgname=vg_server



## System bootloader configuration
#bootloader --location=mbr --boot-drive=nvme0n1
## Partition clearing information
#ignoredisk --only-use=nvme0n1
#clearpart --drives=nvme0n1 --all
#zerombr 
## Disk partitioning information
#part /boot --fstype="ext4" --ondisk=nvme0n1 --size=2048
#part pv.01 --fstype="lvmpv" --ondisk=nvme0n1 --size=1 --grow
#volgroup vg_server --pesize=4096 pv.01
#logvol /  --fstype="xfs" --size=80000 --name=lv_root --vgname=vg_server
## Recommended swap size = (0.5 * RAM) + (overcommit ratio * RAM)
## 0.5 * 24 + ((12 + 12) / 24) * 24 = 36
#logvol swap --fstype="swap" --size=36864 --name=lv_swap --vgname=vg_server




%packages
@base
@core
%end

# System services
services --enabled=NetworkManager,sshd


%post
/usr/sbin/useradd -m -u 1000 -G wheel -d /ansible ansible
echo "ansible	ALL=(ALL)	NOPASSWD:ALL" >> /etc/sudoers
%end

%post
mkdir -p /ansible/.ssh
cat << EOFKEY > /ansible/.ssh/authorized_keys
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDONk0GmAp5oONw86Gng1K0cIlZsJ4nzdoCX896jxX584+ndB1kqwucu3FcBQsU6/ljn6OWrfEE2FDu16ZyRVZdmvF75gCGyJ30CDvJ9Njv+sOIXJjUsAjFHImdomPNN5YWkveSAnk6F//LoOhFXGQ+cZHhENbrFhogNeRFXilO1Dayw8Wb8CjnKhdOwckd0pI0k5G3g+1UdPSq7Fg44gf80mnwCFShgdxhJUFJR0nzoC7Kl8QTx7Db8An2Dn4awu/QXBmg0af7BL5Te35OCdrcmpwL15h/dEc26t/MTE1iNbkrPaJpIOIv5vJHZjiP+NyVwEfClmt735v+4BgC5+wt 
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIVxiFii9RuA2PP3L2/u0cGthFy4dSvPq1KzQglNOrSi
EOFKEY
chown -R ansible:ansible /ansible/.ssh
chmod -R go-rwx /ansible/.ssh
%end


%anaconda
pwpolicy root --minlen=6 --minquality=1 --notstrict --nochanges --notempty
pwpolicy user --minlen=6 --minquality=1 --notstrict --nochanges --emptyok
pwpolicy luks --minlen=6 --minquality=1 --notstrict --nochanges --notempty
%end
