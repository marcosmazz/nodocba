# memlock limits

Sets user memlock limits.

#### Configuration

- `memlock_limits_soft`: Soft limit. Defaults to `unlimited`.
- `memlock_limits_hard`: Hard limit. Defaults to `unlimited`.
