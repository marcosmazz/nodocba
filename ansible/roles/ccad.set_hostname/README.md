# Set hostname

Sets hostname temporarily.

### Configuration

- `set_hostname`: Target hostname. Set to `{{ inventory_hostname }}` by default.

