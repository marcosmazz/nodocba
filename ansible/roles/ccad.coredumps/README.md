# Core dumps

Configures core dump settings.

### Configuration

- `coredumps_core_pattern`: Value to set in `/proc/sys/kernel/core_pattern`. See `man 5 core` for help.
- `coredumps_hard_limit`: Core dump hard ulimit. Default: unlimited.
- `coredumps_soft_limit`: Core dump soft ulimit. Default: 0.
