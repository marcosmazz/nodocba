# Nodocba

Repo para instalación de un solo nodo de cómputo basado en OpenHPC y soporte para CUDA.


`base`: Archivos para la instalación previa a ansible
```
En la linea de boot de la instalación agregar
 inst.ks=https://gitlab.com/marcosmazz/nodocba/raw/main/base/node.ks
```
`ansible`: Playbooks para pasar luego de la instalación base

## TODO:
- Revisar el kickstart. Armar el particionado de los discos
- Habilitar GPG keys en los repos en hostfiles
- Adaptar las hots_vars al nodo en lugar de un vbox-host
- Verificar que anda driver NVIDIA y CUDA
- Cambiar el slurm.conf y gres.conf para un nodo con GPU
- Agregar todos los paquetes user-space que hagan falta en ccad.installpkgs
- Armar rol para agregar compiladores que vienen con OpenHPC
- Armar rol para instalar lmod de OpenHPC 


#Links:
[Guía OpenHPC] (https://github.com/openhpc/ohpc/releases/download/v2.6.GA/Install_guide-Rocky8-Warewulf-SLURM-2.6-x86_64.pdf)


